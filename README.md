# README #

Hello.
This application was created using Qt 4.8.1.

### What is this repository for? ###

* This repo contains 2 folders: "Chess" - containing the source code, & "Executable" - containing a compiled version of the project.

### How do I get set up? ###

* Before running the project, ensure that Qt is installed on your system.
* This should be 4.8.1 or higher.
* I used Qt Creator to develop the project, but Netbeans or any other editor should do. Regardless of the coding platform, ensure that appropriate compilers are installed in your system.

### Who do I talk to? ###

* In case of any queries you may reach me at j.matu@yahoo.com
* Additionally, for queries on the Qt platform, visit: http://doc.qt.io/ Note: You may select the Open Source version, as I developed this as a student.